# Arabic translation of Views Infinite Scroll (8.x-1.5)
# Copyright (c) 2017 by the Arabic translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Views Infinite Scroll (8.x-1.5)\n"
"POT-Creation-Date: 2017-08-19 02:44+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Arabic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=((n==1)?(0):((n==0)?(1):((n==2)?(2):((((n%100)>=3)&&((n%100)<=10))?(3):((((n%100)>=11)&&((n%100)<=99))?(4):5)))));\n"

msgid "Go to next page"
msgstr "الذهاب إلى الصفحة التالية"
msgid "Views"
msgstr "الرؤى"
