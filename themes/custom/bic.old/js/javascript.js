(function ($) {
    /*$(document).ready(function() {
        $("#block-languageswitcher li.en a").text("EN");
        $("#block-languageswitcher li.ar a").text("AR");
    });*/

    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
    });

    
    /*$("#accordion .panel .panel-title:not(:has(.panel-title.current))").find(".panel").hide().end() // Hide all other ULs
    .click(function (e) {
    if (this == e.target) {
        $(this).children('.panel').slideToggle();
        $(this).toggleClass('open');
    }
    $(this).children(".panel-title").text(this.toggle ? "-" : "+");
    return false;
    });*/


})(jQuery); 

(function ($) {

    Drupal.behaviors.navbarx = {

        attach: function (context, settings) {

        var collapsebutton = $(context).find("#navbar .navbar-toggle");

        $(collapsebutton).on('click', function () {

            $(this).toggleClass("collapsed");

            $("body").toggleClass("mobile-menu-expanded");



            // Handle Overlay.

            if ($(this).attr('aria-expanded') == 'true') {

              removeOverlay();

            }

            else {

               createOverlay();

            }

        });

        }

    };

})(jQuery);    

// Create overlay.

function createOverlay() {

    jQuery("<div id='overlay-mobile'></div>").appendTo("body").on('click',function () {
  
      jQuery("#navbar .navbar-toggle").trigger("click");
  
    });
  
  }
  
  
  
  // Remove overlay.
  
  function removeOverlay() {
  
    jQuery("#overlay-mobile").remove();
  
  }